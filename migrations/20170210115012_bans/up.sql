CREATE TABLE bans (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  user_id BIGINT NOT NULL,
  ban_started BIGINT NOT NULL DEFAULT CURRENT_TIMESTAMP -- diesel has no sqlite3 `datetime` support
)
