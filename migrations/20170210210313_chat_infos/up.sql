CREATE TABLE chat_infos (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  chat_id BIGINT NOT NULL,
  share_link VARCHAR NOT NULL,
  created BIGINT NOT NULL DEFAULT CURRENT_TIMESTAMP -- diesel has no sqlite3 `datetime` support
)