use schema::bans;
use schema::user_infos;
use schema::chat_infos;

#[derive(Queryable)]
pub struct Ban {
    pub id: i32,
    pub user_id: i64,
    pub ban_started: i64
}

#[derive(Insertable)]
#[table_name="bans"]
pub struct NewBan {
    pub user_id: i64
}

#[derive(Queryable)]
pub struct UserInfo {
    pub id: i32,
    pub user_id: i64,
    pub chat_id: i64,
    pub created: i64
}

#[derive(Insertable)]
#[table_name="user_infos"]
pub struct NewUserInfo {
    pub user_id: i64,
    pub chat_id: i64
}

#[derive(Queryable)]
pub struct ChatInfo {
    pub id: i32,
    pub chat_id: i64,
    pub share_link: String,
    pub created: i64
}

#[derive(Insertable)]
#[table_name="chat_infos"]
pub struct NewChatInfo {
    pub chat_id: i64,
    pub share_link: String
}