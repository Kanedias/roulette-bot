CREATE TABLE user_infos (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  user_id BIGINT NOT NULL,
  chat_id BIGINT NOT NULL,
  created BIGINT NOT NULL DEFAULT CURRENT_TIMESTAMP -- diesel has no sqlite3 `datetime` support
)