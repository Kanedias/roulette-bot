
extern crate tg_botapi;
extern crate chrono;
extern crate rand;

#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_codegen;

use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::thread;
use std::time::Duration;
use std::sync::Arc;
use std::sync::Mutex;
use std::ops::Deref;

use chrono::Local;

use rand::Rng;

use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;

use tg_botapi::BotApi;
use tg_botapi::args::*;
use tg_botapi::types::*;

use models::ChatInfo;

pub mod schema;
pub mod models;

struct BotData {
    bot_id: i64,
    chat_infos: HashMap<i64, ChatInfo>,
    connection: Arc<Mutex<SqliteConnection>>
}

fn main() {
    // read the bot token from file
    let bt_file = File::open("bot_token.txt").expect("Couldn't open bot_token.txt file!");
    let mut reader = BufReader::new(&bt_file);
    let mut token_str = String::new();
    reader.read_line(&mut token_str).expect("Couldn't read bot token from file");

    // initialize Telegram Bot API
    let api = Arc::new(BotApi::new_debug(&token_str));
    let me = api.get_me().unwrap();

    // read bot data from storage
    let conn = SqliteConnection::establish("data/bot.db").expect("Error connecting to sqlite3 db!");
    
    let mut bot_data = BotData { chat_infos: HashMap::new(), bot_id: me.id, connection: Arc::new(Mutex::new(conn)) };
    let infos = schema::chat_infos::table.load::<ChatInfo>(bot_data.connection.lock().unwrap().deref()).expect("Couldn't load chat infos!");
    for info in infos {
        bot_data.chat_infos.insert(info.chat_id, info);
    }
    // bot_data.chat_info.insert(1000, ChatInfo {share_link: String::from("https://t.me/joinchat/AAAAAD32dpl7ytnPcU_J4g")});

    println!("Starting bot listener for {}", &me.first_name);
    let mut update_args = GetUpdates::new().timeout(1).offset(0);
    loop {
        println!("Heartbeat - time {}", Local::now());
        let updates = api.get_updates(&update_args).unwrap();
        for update in updates {
            println!("Got update: {}", update.update_id);
            update_args.offset = Some(update.update_id + 1);
            if let Some(message) = update.message {
                handle_update(&api, &mut bot_data, message)
            }
        }
    }
}

fn handle_update(api: &Arc<BotApi>, data: &mut BotData, m: Message) {
    if m.chat.type_name == "channel" {
        println!("Skipping channel message, not supported...");
        return
    }

    println!("Got message from chat: {:?}", &m.chat);
    // this is not a channel, from field must be present
    let sender = m.from.unwrap();
    let chat_id = m.chat.id;
    let text_passed = m.text.unwrap_or(String::new());
    let mut split_text = text_passed.split_whitespace();

    // we can't kick members if this option is present
    if m.chat.all_members_are_administrators.unwrap_or(false) {
        let _ = api.send_message(&SendMessage::new("You should turn off 'All members are admins' \
                                                    option in order to use this bot, \
                                                    or it won't be able to kick anyone")
                .chat_id(chat_id)
                .reply_to_message_id(m.message_id));
        return
    }

    if let Some(cmd) = split_text.next() {
        match cmd {
            "/start" | "/start@kickroulette_bot" => {
                if m.chat.type_name != "private" {
                    return
                }

                let _ = schema::create_user_info(data.connection.lock().unwrap().deref(), chat_id, sender.id);
                let _  = api.send_message(&SendMessage::new("Hi! This chat will be used for re-inviting of you in case you got banned!")
                            .chat_id(chat_id)
                            .parse_mode("Markdown")
                            .reply_to_message_id(m.message_id));
            }
            "/setlink" | "/setlink@kickroulette_bot" => {
                if let Some(chat_invite_link) = split_text.next() {
                    // caller should be an administrator of the group to be able to set link
                    let admins = api.get_chat_administrators(&GetChatAdministrators::new().chat_id(chat_id));
                    if !admins.is_ok() {
                        let _ = api.send_message(&SendMessage::new("Couldn't get chat admins, sorry")
                                .chat_id(chat_id)
                                .reply_to_message_id(m.message_id));
                        return
                    }

                    if !admins.unwrap().iter().any(|v| v.user.id == sender.id) {
                        let _ = api.send_message(&SendMessage::new("You should be admin of the group for doing that!")
                                .chat_id(chat_id)
                                .reply_to_message_id(m.message_id));
                        return
                    }

                    // he is the administrator, do the requested
                    
                    let new_info = schema::create_chat_info(data.connection.lock().unwrap().deref(), chat_id, String::from(chat_invite_link));
                    data.chat_infos.insert(chat_id, new_info);
                    let _  = api.send_message(&SendMessage::new("Invite link set!")
                            .chat_id(chat_id)
                            .parse_mode("Markdown")
                            .reply_to_message_id(m.message_id));
                }
            }
            "/roll" | "/roll@kickroulette_bot" => {
                use schema::chat_infos;

                // check if we have chat info for this chat
                let chat_info = chat_infos::table.filter(chat_infos::chat_id.eq(chat_id))
                                                 .first::<models::ChatInfo>(data.connection.lock().unwrap().deref());
                if chat_info.is_err() {
                    let _ = api.send_message(&SendMessage::new("Creator of this chat should execute /setlink for this chat first!")
                            .chat_id(chat_id)
                            .reply_to_message_id(m.message_id));
                    return
                }

                // get list of admins of this chat
                let admins = api.get_chat_administrators(&GetChatAdministrators::new().chat_id(chat_id));
                if !admins.is_ok() {
                    let _ = api.send_message(&SendMessage::new("Couldn't get chat admins, sorry")
                            .chat_id(chat_id)
                            .reply_to_message_id(m.message_id));
                    return
                }

                // check the sender is not a creator of this chat
                /*
                if admins.as_ref().unwrap().iter().any(|v| v.user.id == sender.id && v.status == "creator") {
                    let _ = api.send_message(&SendMessage::new("Creator of the chat can't be banned and so can't play, sorry")
                            .chat_id(chat_id)
                            .reply_to_message_id(m.message_id));
                    return
                }
                */

                // check the bot is in admins
                if !admins.unwrap().iter().any(|v| v.user.id == data.bot_id) {
                    let _ = api.send_message(&SendMessage::new("You should make this bot admin of this group for doing that!")
                            .chat_id(chat_id)
                            .reply_to_message_id(m.message_id));
                    return
                }

                // all good, we're admin
                let mut generator = rand::thread_rng();
                let roll = generator.gen_range::<u8>(0, 6) + 1; // 1/2/3/4/5/6
                let _ = api.send_message(&SendMessage::new(format!("{} rolls {}!", sender.first_name, roll).as_str())
                        .chat_id(chat_id)
                        .reply_to_message_id(m.message_id));
                if roll == 6 {
                    // kick the player
                    let _ = api.send_message(&SendMessage::new("Sorry, you are dead and will be kicked in 15 secs. \
                                                                Open a chat with me and wait for an hour, \
                                                                I'll unban & invite you back. Bye!")
                            .chat_id(chat_id)
                            .reply_to_message_id(m.message_id));
                    
                    // store ban for the case if bot gets killed
                    let sender_id = sender.id;
                    schema::create_ban(data.connection.lock().unwrap().deref(), sender_id);

                    // spawn unban-thread
                    let (api_ref, conn_ref, ban_msg_id) = (api.clone(), data.connection.clone(), m.message_id);
                    thread::spawn(move || {
                        use schema::user_infos;

                        thread::sleep(Duration::from_secs(60));
                        let locked_conn = conn_ref.lock().unwrap();
                        let _ = api_ref.unban_chat_member(&UnbanChatMember { chat_id: Some(chat_id), user_id: sender_id, chat_username: None });
                        let user_info = user_infos::table.filter(user_infos::user_id.eq(sender_id))
                                                          .order(user_infos::id.desc())
                                                          .first::<models::UserInfo>(locked_conn.deref()); 
                        match user_info {
                            Ok(user_chat) => api_ref.send_message(&SendMessage::new(format!("Rejoin here! {}", chat_info.unwrap().share_link).as_str())
                                                    .chat_id(user_chat.chat_id)),
                            Err(_) => api_ref.send_message(&SendMessage::new(format!("Banned user didn't open chat with bot, \
                                                                                      please reinvite him manually! {}", 
                                                                                      sender.username.unwrap_or(sender.first_name)).as_str()) // 
                                                    .chat_id(chat_id)
                                                    .reply_to_message_id(ban_msg_id))
                        }
                    });

                    // wait in another thread, don't break the heartbeat
                    let api_ref = api.clone();
                    thread::spawn(move || {
                        thread::sleep(Duration::from_secs(15));
                        let _ = api_ref.kick_chat_member(&KickChatMember { chat_id: Some(chat_id), user_id: sender_id, chat_username: None });
                    });
                }
            }
            _ => {}
        }
    }
}
