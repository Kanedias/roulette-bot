infer_schema!("data/bot.db");

use models::*;

use diesel;
use diesel::prelude::*;
use diesel::sqlite::SqliteConnection;

pub fn create_ban(conn: &SqliteConnection, user_id: i64) -> Ban {
    let new_ban = NewBan { user_id: user_id };
    let rows_affected = diesel::insert(&new_ban).into(bans::table).execute(conn);
    if rows_affected.is_err() {
        // shouldn't happen
        panic!("Couldn't insert new ban!");
    }

    bans::table.order(bans::id.desc()).first::<Ban>(conn).expect("Cannot query last inserted ban!")
}


pub fn create_chat_info(conn: &SqliteConnection, chat_id: i64, share_link: String) -> ChatInfo {
    let new_chat_info = NewChatInfo { chat_id: chat_id, share_link: share_link };
    let rows_affected = diesel::insert(&new_chat_info).into(chat_infos::table).execute(conn);
    if rows_affected.is_err() {
        // shouldn't happen
        panic!("Couldn't insert new chat info!");
    }

    chat_infos::table.order(chat_infos::id.desc()).first::<ChatInfo>(conn).expect("Cannot query last inserted chat info!")
}

pub fn create_user_info(conn: &SqliteConnection, chat_id: i64, user_id: i64) -> UserInfo {
    let new_user_info = NewUserInfo { chat_id: chat_id, user_id: user_id };
    let rows_affected = diesel::insert(&new_user_info).into(user_infos::table).execute(conn);
    if rows_affected.is_err() {
        // shouldn't happen
        panic!("Couldn't insert new user info!");
    }

    user_infos::table.order(user_infos::id.desc()).first::<UserInfo>(conn).expect("Cannot query last inserted chat info!")
}